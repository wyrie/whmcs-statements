<?php
$pdf->SetAuthor($CONFIG['CompanyName']);
$pdf->SetMargins(5, 10, 5);
$pdf->SetFooterMargin(10);
$pdf->SetAutoPageBreak(TRUE, 10);
$pdf->SetPrintHeader(false);
$pdf->setPrintFooter(false);
$pdf->AddPage();

# Logo
if (file_exists(ROOTDIR.'/assets/img/logo.png')) $pdf->Image(ROOTDIR.'/assets/img/logo.png',5,5,45);
elseif (file_exists(ROOTDIR.'/assets/img/logo.jpg')) $pdf->Image(ROOTDIR.'/assets/img/logo.jpg',5,5,45);
else $pdf->Image(ROOTDIR.'/assets/img/placeholder.png',5,5,75);

# Company Details
$pdf->SetXY(-50, 5);
$pdf->SetFont('freesans', '', 8);
$pdf->MultiCell(45, 27, $CONFIG['InvoicePayTo'], 0, 'R');
$pdf->Ln(5);

# Title
$pdf->SetFillColor(255);
$pdf->SetFont('freesans', 'B', 16);
$pdf->SetTextColor(0);
$pdf->Cell(0, 0, strtoupper($_ADDONLANG['pdf_title']), 0, 1, 'C', '1');
$pdf->SetTextColor(0);
$pdf->Ln(5);

# Clients Details
$this_row = $pdf->GetY();
$address_to = (!empty($client->companyname)) ? $client->companyname : $client->firstname . ' ' . $client->lastname;
$client_details = '<b>'. $address_to . '</b>' . str_repeat('<br />', 2);

$address = trim($client->address1, "\r\n") . PHP_EOL;
$address .= trim($client->address2, "\r\n") . PHP_EOL;
foreach(array('city','state','postcode','country') as $prop) {
  if (!empty($client->{$prop})) {
    $address .= $client->{$prop} . PHP_EOL;
  }
}
$client_details .= nl2br($address); 

$pdf->SetXY(5, $this_row);
$pdf->SetFont('freesans', '', 8);
$pdf->MultiCell(90, 35, $client_details, 0, 'L', false, 1, '', '', true, 0, true);  

# Statement date
$pdf->SetXY(-95, $this_row);
$pdf->MultiCell(90, 35, 'Date:    '.date('d/m/Y'), 0, 'R', false, 1, '', '', true, 0, true);
$pdf->Ln(5);

# Transactions table
$html = <<<HTML
<table width="100%" bgcolor="#ccc" cellspacing="1" cellpadding="2" border="0"> 
  <tr>
    <th align="center">{$_ADDONLANG['view_date']}</th>
    <th align="center">{$_ADDONLANG['view_reference']}</th>
    <th align="center">{$_LANG['invoicesdescription']}</th>
    <th align="center">{$_ADDONLANG['view_debit']}</th>
    <th align="center">{$_ADDONLANG['view_credit']}</th>
    <th align="center">{$_ADDONLANG['view_balance']}</th>
  </tr>
HTML;

$transactions = '';
foreach($tx as $k => $_t) {
  $transactions .= '<tr>'.
          '<td class="text-center">'.$_t['date'].'</td>'.
          '<td class="text-center">'.$_t['reference'].'</td>'.
          '<td>'.$_t['description'].'</td>'.
          '<td class="text-right">'.$_t['debit'].'</td>'.
          '<td class="text-right">'.$_t['credit'].'</td>'.
          '<td class="text-right">'.$_t['balance'].'</td>'.
          '</tr>';

# Ageing
$pdf->writeHTML($html, true, false, false, false, '');
