<?php
namespace WHMCS\Module\Addon\Statements\Admin;
use WHMCS\Module\Addon\Statements\BaseController;
use WHMCS\Database\Capsule;

class Controller extends BaseController
{
  public function index($vars)
  {
    if (is_null($this->_mlink)) {
      $this->_mlink = $vars['modulelink'];
    }
    if (is_null($this->_lang)) {
      $this->_lang  = $vars['_lang'];
    }
    
    $fixed_date = $sending_date = $group_payments = $include_ageing = $attach_invoices = $enable_schedule = '';
    $sending_date_options = '';

    $_settings = $this->getSettings();
    if (count($_POST) && isset($_POST['token']) && isset($_POST['btnsubmit']) ) {
      foreach($_settings as $k => $v) {
        $value = (isset($_POST[$k])) ? $_POST[$k] : '';
        Capsule::table('tbladdonmodules')
          ->where('module', '=', 'statements')
          ->where('setting', '=', $k)
          ->update(['value' => $value]);
      }
      $_settings = $this->getSettings('all', true);
    }

    foreach($_settings as $setting => $value) {
      switch ($setting) {
        case 'group_payments':
        case 'include_ageing':
        case 'attach_invoices':
        case 'enable_schedule':
          if (!empty($value)) {
            ${$setting} = 'checked="checked"';
          }
          elseif ($setting == 'enable_schedule') {
            $sending_date = 'disabled="disabled"';
          }
          break;
        case 'sending_date':
          foreach(array('signup','fixed') as $_opt) {
            $option = '<option value="'.$_opt.'" ';
            if ($_opt == $value) {
              $option .= 'selected="selected"';
            }
            $option .= '>'.$this->_lang['senddate_'.$_opt].'</option>';
            $sending_date_options .= $option . PHP_EOL;
          }
          break;
        case 'fixed_date':
          $fixed_date_value = $value;
          break;
      }
    }

    return <<<EOF
<form id="settingsform" method="post" class="form-horizontal" action="{$this->_mlink}&amp;action=index">
<div class="page-header">
  <h2>{$this->_lang['display_header']}</h2>
</div>

<div class="form-group">
  <label class="col-sm-2 control-label">{$this->_lang['group_label']}</label>
  <div class="col-sm-3">
    <div class="checkbox">
      <label>
        <input id="group_payments" name="group_payments" $group_payments type="checkbox" />
        {$this->_lang['group_help']}
      </label>
    </div>
  </div>
</div>
<div class="form-group">
  <label class="col-sm-2 control-label">{$this->_lang['ageing_label']}</label>
  <div class="col-sm-3">
    <div class="checkbox">
      <label>
        <input id="include_ageing" name="include_ageing" $include_ageing type="checkbox" />
        {$this->_lang['ageing_help']}
      </label>
    </div>
  </div>
</div>

<div class="page-header">
  <h2>{$this->_lang['sending_header']}</h2>
</div>
<div style="margin-top:30px;">        
  <div class="alert alert-info" role="alert">
    <i class="fa fa-info-circle fa-lg"></i> {$this->_lang['notice_msgtpl']}
  </div>
</div>
<div class="form-group">
  <label class="col-sm-2 control-label">{$this->_lang['attach_label']}</label>
  <div class="col-sm-3">
    <div class="checkbox">
      <label>
        <input id="attach_invoices" name="attach_invoices" $attach_invoices type="checkbox" />
        {$this->_lang['attach_help']}
      </label>
    </div>
  </div>
</div>
<div class="form-group">
  <label class="col-sm-2 control-label">{$this->_lang['sendenable_label']}</label>
  <div class="col-sm-3">
    <div class="checkbox">
      <label>
        <input id="enable_schedule" name="enable_schedule" $enable_schedule type="checkbox" />
        {$this->_lang['sendenable_help']}
      </label>
    </div>
  </div>
</div>
<div class="form-group">
  <label class="col-sm-2 control-label">{$this->_lang['senddate_label']}</label>
  <div class="col-sm-3">
    <select class="form-control" id="sending_date" name="sending_date" $sending_date>
      $sending_date_options
    </select>
    <span class="help-block"></span>
    <div id="fixed_date_container">
      <input type="text" id="fixed_date" name="fixed_date" value="$fixed_date_value" class="form-control date-picker">
      <span class="help-block"></span>
    </div> 
  </div>
</div>
<hr />
<div class="row">
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <input name="btnsubmit" value="Save Changes" class="btn btn-primary" type="submit" />
    </div>
  </div>
</div>
</form>
EOF;
  }

  public function getclients($vars)
  {
    $clients = array();
    $query = Capsule::table('tblclients')->select('id','firstname','lastname','companyname');

    if (isset($_POST['dropdownsearchq']) && !empty($_POST['dropdownsearchq'])) {
      $s = filter_var($_POST['dropdownsearchq']);
      $query->whereRaw("CONCAT(firstname, ' ', lastname) LIKE ?", array('%'.$s.'%'))
        ->orWhere('email', 'LIKE', '%'.$s.'%')
        ->orWhere('companyname', 'LIKE', '%'.$s.'%');
    }
    
    $results = $query->get();
    foreach($results as $cobj) {
      $clients[] = array('id' => $cobj->id, 'name' => $cobj->firstname . ' ' . $cobj->lastname, 'companyname' => $cobj->companyname, 'email' => $cobj->email);
    }

    echo json_encode($clients);
    exit;
  }

  public function view($vars) 
  {
    global $_ADMINLANG;
    
    if (is_null($this->_mlink)) {
      $this->_mlink = $vars['modulelink'];
    }
    if (is_null($this->_lang)) {
      $this->_lang  = $vars['_lang'];
    }
    
    $userid_options = '';
    if (isset($_GET['userid']) && is_numeric($_GET['userid'])) {
      $_id = $_GET['userid'];
      $_client = Capsule::table('tblclients')
             ->where('id', '=', $_id)
             ->select('id','firstname','lastname','companyname')
             ->get();
       
      if (count($_client)) {
        $userid_options .= "<option value=\"{$_client[0]->id}\">{$_client[0]->firstname} {$_client[0]->lastname} ({$_client[0]->companyname})</option>\n";
        
        $daterange_options = '';
        $daterange_entries = array('current_month', 'last_month', 'custom_range');
        foreach ($daterange_entries as $_opt) {
          $daterange_options .= '<option value="'.$_opt.'"';
          if (!empty($_REQUEST['date_range']) && in_array($_REQUEST['date_range'], $daterange_entries) && ($_REQUEST['date_range'] == $_opt)) {
            $daterange_options .= ' selected="selected"';
          }
          $daterange_options .= '>'.$this->_lang['daterange_options'][$_opt].'</option>';
        }

        $args = array($_id);
        if (!empty($_POST['daterange_from'])) {
           $daterange_from = $_POST['daterange_from'];
           $dofrom = date_create_from_format('d/m/Y', $daterange_from); 
           $args[] = $dofrom->getTimestamp();
        } else {
          $daterange_from = '';
        }

        if (!empty($_POST['daterange_to'])) {
          $daterange_to = $_POST['daterange_to'];
          $doto = date_create_from_format('d/m/Y', $daterange_to);
          $args[] = $doto->getTimestamp();
        } else {
          $daterange_to = '';
        }

        if (!empty($_REQUEST['date_range']) && $_REQUEST['date_range'] == 'last_month') {
          $args = array_merge($args, array(
            strtotime("first day of previous month"),
            strtotime("last day of previous month")
          ));
        }
        
        $tx = call_user_func_array(array($this, "getTransactions"), $args);
        if (!count($tx)) {
          $transactions = '<tr><td colspan="6">No Records Found</td></tr>'; 
        } else {
          $transactions = <<<THEADERS
          <tr>
            <th>{$_ADMINLANG['fields']['date']}</th>
            <th>{$this->_lang['view_reference']}</th>
            <th>{$_ADMINLANG['fields']['description']}</th>
            <th>{$this->_lang['view_debit']}</th>
            <th>{$this->_lang['view_credit']}</th>
            <th>{$_ADMINLANG['fields']['balance']}</th>
          </tr>
THEADERS;
        }

        foreach($tx as $k => $_t) {
          $transactions .= '<tr>'.
          '<td class="text-center">'.$_t['date'].'</td>'.
          '<td class="text-center">'.$_t['reference'].'</td>'.
          '<td>'.$_t['description'].'</td>'.
          '<td class="text-right">'.$_t['debit'].'</td>'.
          '<td class="text-right">'.$_t['credit'].'</td>'.
          '<td class="text-right">'.$_t['balance'].'</td>'.
          '</tr>';
        }
      }
    }

  $html = <<<HEADER
<form id="viewstatement" method="post" action="{$this->_mlink}&amp;action=view">
<div class="row">
  <div class="col-md-8">
    <table class="form" width="100%" border="0" cellspacing="2" cellpadding="3">
      <tr>
        <td width="130" class="fieldlabel">{$_ADMINLANG['fields']['client']}</td>
      <td class="fieldarea">
        <select id="selectUserid" name="userid" class="form-control selectize selectize-client-search" placeholder="{$_ADMINLANG['global']['typeToSearchClients']}" data-value-field="id">
        $userid_options
        </select>
      </td>
    </tr>
   </table>
  </div>
</div>
HEADER;

  if (isset($_id))
  {
    $html .= <<<BODY
<div class="row" style="margin-top: 40px">
<div class="col-md-12">
  <div class="pull-left">
        {$_ADMINLANG['transactions']['show']}:&nbsp;  <select id="date_range" name="date_range">$daterange_options</select>
        <div id="daterange_container" style="margin-left: 10px" class="input-inline"><input type="text" name="daterange_from" value="$daterange_from" class="date-picker" /> to  <input type="text" name="daterange_to" value="$daterange_to" class="date-picker" />
        <input type="submit" value="{$_ADMINLANG['global']['submit']}" />
        </div>
  </div>
  <div class="pull-right">
    <div class="btn-group btn-group-sm" role="group">
        <button id="viewInvoiceAsClientButton" type="button" class="btn btn-default" onclick="window.open('/viewinvoice.php?id=1&amp;view_as_client=1','clientInvoice','')">
          <i class="fa fa-clipboard"></i> {$_ADMINLANG['invoices']['viewAsClient']}
        </button>
        <button id="downloadPdf" type="button" class="btn btn-default" onclick="window.open('{$this->_mlink}&action=download&userid=$_id', 'pdfd', '')">
          <i class="fa fa-download"></i> {$_ADMINLANG['invoices']['downloadpdf']}
        </button>
    </div>
  </div>
  <br />
  <div class="tablebg">
    <table id="sortabletbl1" class="datatable" width="100%" border="0" cellspacing="1" cellpadding="3">
      $transactions
    </table>
  </div>
</div>
</div>
</form>
BODY;

    $include_ageing = (bool) $this->getSettings('include_ageing');
    if ($include_ageing) {
      $_ageing = $this->getAgeing();
      $html .= <<<AGEING
      <div class="row" style="margin-top: 80px">
        <div class="col-md-12">
          <table id="sortabletbl2" class="datatable" width="100%" border="0" cellspacing="1" cellpadding="3">
            <tr>
              <th>{$this->_lang['ageing_cat5']}</th>
              <th>{$this->_lang['ageing_cat4']}</th>
              <th>{$this->_lang['ageing_cat3']}</th>
              <th>{$this->_lang['ageing_cat2']}</th>
              <th>{$this->_lang['ageing_cat1']}</th>
              <th>{$this->_lang['ageing_due']}</th>
              <td class="text-right">{$_ageing['amount_due']}</td>
            </tr>
            <tr>
              <td class="text-center">{$_ageing['120d']}</td>
              <td class="text-center">{$_ageing['90d']}</td>
              <td class="text-center">{$_ageing['60d']}</td>
              <td class="text-center">{$_ageing['30d']}</td>
              <td class="text-center">{$_ageing['current']}</td>
              <th>{$this->_lang['ageing_paid']}</th>
              <td class="text-right">{$_ageing['amount_paid']}</td>
            </tr>
          </table>
        </div>
      </div>
AGEING;
    }
   }

  return $html;
  }

  public function download($vars) {
    //TODO
  }
}
