<?php
namespace WHMCS\Module\Addon\Statements;
use WHMCS\Database\Capsule;

class BaseController
{
  protected $_lang         = null;
  protected $_mlink        = null;
  protected $_settings     = null;
  protected $_startdate    = null;
  protected $_enddate      = null;
  private   $_ageing       = null;
  private   $_invoices     = null;
  private   $_transactions = null;

  /**
   * getTransactions
   * Fetch all the transactions that will be displayed
   * on the account statement, for a date range (defaults
   * to current month).
   *
   * Possible transactions are invoices, payments allocated
   * to invoices and account credits.
   *
   * @param  integer $userid
   * @param  string  $startdate
   * @param  string  $enddate
   * @return array
   * @author Scott Barr <gsbarr@gmail.com>
   **/
  protected function getTransactions($userid, $startdate=null, $enddate=null)
  {
    global $_lang, $CONFIG;

    $_id = (int)$userid;
    $_dateformat = preg_replace(array('/DD/','/MM/','/YYYY/'), array('d','m','Y'), $CONFIG['DateFormat']);

    // Determine the date range.
    $this->_startdate = (is_null($startdate)) ? strtotime('first day of this month') : $startdate;
    $this->_enddate = (is_null($enddate)) ? strtotime('last day of this month') : $enddate;

    // Set account currency.
    $_client = Capsule::table('tblclients')
      ->where('id', '=', $_id)
      ->select('currency')
      ->get();

    $_currdb = getCurrency($_id);
    $_currid = $_currdb['id'];

    $include_ageing = (bool) $this->getSettings('include_ageing');

    // Collect ALL the transactions for this account.
    // We will apply date filters later, we need the 
    // data for the opening balance.
    $this->_invoices = Capsule::table('tblinvoices')
      ->where('userid', '=', $_id)
      ->orderBy('date', 'asc')
      ->get();

    $count = 0;
    foreach ($this->_invoices as $_inv) {
      // Check if this invoice was a mass pay invoice,
      // in which case we don't want to add it as an
      // invoice here
      $_ii = Capsule::table('tblinvoiceitems')
        ->where('invoiceid', '=', $_inv->id)
        ->where(function ($query) {
                $query->where('type', '=', 'AddFunds')
                      ->orWhere('type', '=', 'Invoice');
            })
        ->get();

      if (is_array($_ii) && !count($_ii))
      {
        $total = $_inv->credit + $_inv->total;
        $ref   = (!empty($_inv->invoicenum)) ? $_inv->invoicenum : "#{$_inv->id}";

        $tstamp = strtotime($_inv->date);
        $this->_transactions[$tstamp."_$count"] = array(
          'date' => fromMySQLDate($_inv->date, false, true),
          'reference' => $ref,
          'description' => $this->_lang['invoice'],
          'debit' => $total,
          'credit' => 0
        );
        $count++;
      }
    }

    $_ac = Capsule::table('tblaccounts')
      ->where('userid', $_id)
      ->orderBy('date', 'asc')
      ->get();

    foreach($_ac as $_a)
    {
      $_ii = Capsule::table('tblinvoiceitems')
        ->where('invoiceid', '=', $_a->invoiceid)
        ->select('type', 'relid')
        ->get();

      $tstamp = strtotime(date_create_from_format('Y-m-d H:i:s', $_a->date)->format('d F Y'));
      $transaction_key = $tstamp."_$count";
      if ($_ii[0]->type == 'AddFunds') {
        $description = $this->_lang['itemtype_addfunds'];
      }
      elseif ($_ii[0]->type == 'Invoice') {
        $description = $this->_lang['itemtype_invoice'] . ' - ';
        $invs = array();
        foreach($_ii as $_i) {
          $_in = Capsule::table('tblinvoices')->find($_i->relid, array('invoicenum'));
          $invs[] = (!empty($_in->invoicenum)) ? $_in->invoicenum : "#{$_i->relid}";
        }
        $description .= implode(', ', $invs);
      } else {
        $description = $_a->description;
        if (!empty($_a->invoiceid)) {
          $_in = Capsule::table('tblinvoices')->find($_a->invoiceid, array('invoicenum'));
          $ref = (!empty($_in->invoicenum)) ? $_in->invoicenum : '#' . $_a->invoiceid;
          $description .= ' - ' . $ref;
        }
      }

      $this->_transactions[$transaction_key] = array(
        'date' => fromMySQLDate($_a->date, false, true),
        'reference' => $_a->transid,
        'description' => $description,
        'debit' => $_a->amountout,
        'credit' => $_a->amountin
      );
      $count++;

    }

    ksort($this->_transactions);

    $opening_balance = false;
    $balance = $debits = $credits = 0;

    foreach($this->_transactions as $k => $row)
    {
      list($_transdate,) = explode('_', $k);
      if ( ($_transdate >= $this->_startdate) && $opening_balance === false ) {
        $opening_balance = $balance;
      }
      if ($_transdate <= $this->_enddate) {
        $this->ageTransaction($_transdate, $row);

        $debits -= $row['debit'];
        $credits += $row['credit'];
        $balance += $row['credit'] - $row['debit'];
      }

      if ( ($_transdate >= $this->_startdate) && ($_transdate <= $this->_enddate) ) {
        $this->_transactions[$k]['debit'] = ($row['debit'] != 0) ? formatCurrency($row['debit'], $_currid) : '';
        $this->_transactions[$k]['credit'] = ($row['credit'] != 0) ? formatCurrency($row['credit'], $_currid) : '';
        $this->_transactions[$k]['balance'] = formatCurrency($balance, $_currid);
      } else {
        unset($this->_transactions[$k]);
      }
    }

    $this->_ageing['amount_paid'] = $credits;
    $this->_ageing['amount_due'] = ($balance < 0) ? abs($balance) : 0;

    array_unshift($this->_transactions, array(
      'date' => fromMySQLDate(date('Y-m-d', $this->_startdate), false, true),
      'reference' => '',
      'description' => $this->_lang['opening_balance'],
      'debit' => '',
      'credit' => '',
      'balance' => formatCurrency($opening_balance, $currid)
    ));

    return $this->_transactions;
  }

  protected function getSettings($setting=null, $refresh=false)
  {
    if (is_null($this->_settings) || $refresh) {
      $robjs = Capsule::table('tbladdonmodules')
         ->where('module', '=', 'statements')
         ->whereNotIn('setting', ['version', 'access'])
         ->get();
      foreach($robjs as $r) {
        $this->_settings[$r->setting] = $r->value;
      }
    }

    return (!is_null($setting) && array_key_exists($setting, $this->_settings)) ? $this->_settings[$setting] : $this->_settings;
  }

  protected function getAgeing() {
    foreach ($this->_ageing as $period => $amount) {
      $this->_ageing[$period] = (string) formatCurrency($amount, $currid);
    }

    return $this->_ageing;
  }

  protected function findInvoiceByRef($ref) {
    if (substr($ref,0,1) == '#') {
      $column = 'id';
      $ref = substr($ref, 1);
    } else {
      $column = 'invoicenum';
    }

    $inv = false;
    foreach ($this->_invoices as $_i) {
      if($_i->{$column} == $ref) {
        $inv = $_i;
        break;
      }
    }
    return $inv;
  }

  protected function ageTransaction(int $tdate, array $transaction)
  {
    if (is_null($this->_ageing)) {
      $this->_ageing = array('current' => 0, '30d' => 0, '60d' => 0, '90d' => 0, '120d' => 0, 'amount_due' => 0, 'amount_paid' => 0);
    }

    $inv = $this->findInvoiceByRef($transaction['reference']);
    if (!$inv || $inv->status != 'Unpaid') {
      return false;
    }

    $periods = array(
      'current' => array('start' => strtotime('first day of this month', $this->_enddate), 'end' => strtotime('last day of this month', $this->_enddate)),
      '30d'     => array('start' => strtotime('first day of last month', $this->_enddate), 'end' => strtotime('last day of last month', $this->_enddate)),
      '60d'     => array('start' => strtotime('first day of -2 month', $this->_enddate), 'end' => strtotime('last day of -2 month', $this->_enddate)),
      '90d'     => array('start' => strtotime('first day of -3 month', $this->_enddate), 'end' => strtotime('last day of -3 month', $this->_enddate)),
      '120d'    => array('start' => strtotime('first day of -4 month', $this->_enddate), 'end' => 0)
    );
    $balance = 0;
    foreach($periods as $k => $p) {
      if ( ($tdate <= $p['end']) && ($tdate >= $p['start']) ) {
        $this->_ageing[$k] += $transaction['debit'] - $transaction['credit'];
      }
    }
  }
}
