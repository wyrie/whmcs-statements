<?php
add_hook('AdminAreaFooterOutput', 1, function(array $params) {
  if ($params['filename'] == 'clientssummary') {
    return <<<HTML
<script type="text/javascript">
  $( document ).ready(function() {
    $("a[href*='reports.php?report=client_statement']").attr('href', 'addonmodules.php?module=statements&action=view&userid={$params['clientsdetails']['userid']}');
  });
</script>
HTML;
  }

  if ( ($params['filename'] == 'addonmodules') && (isset($_REQUEST['module']) && $_REQUEST['module'] == 'statements') ) {
    return '<script type="text/javascript" src="../modules/addons/statements/assets/js/admin.js"></script>';
  }
});
