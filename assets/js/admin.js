$(function(){
  params={};location.search.replace(/[?&]+([^=&]+)=([^&]*)/gi,function(s,k,v){params[k]=v})

  if (!("action" in params) || params['action'] == 'index') {
    toggle_fixed_date();
    toggle_send_on();
    $('#sending_date').change(function() {
      $('#sending_date option:selected').each(function() {
        toggle_fixed_date();
      });
    });
    $('#enable_schedule').change(function() {
      toggle_send_on();
    });
  }
  else {
    switch (params['action']) {
      case 'view':
        $.getScript("../assets/js/AdminClientDropdown.js", function() {});        
        toggle_daterange_custom();
        $("#selectUserid").change(function() {
          window.location = 'addonmodules.php?module=statements&action=view&userid=' + $("#selectUserid").val();
        });
        $("#date_range").change(function() {
          if ($("#date_range").val() == 'custom_range') {
            toggle_daterange_custom();
          } else {
            window.location = 'addonmodules.php?module=statements&action=view&userid=' + $("#selectUserid").val() + '&date_range=' + $("#date_range").val();
          }
        });
        $("#viewstatement").submit(function(event) {
          if (!($("#date_range").val() == 'custom_range')) {
            return false;
          } else {
            var dfrom = $("input[name=daterange_from]").val();
            var dto = $("input[name=daterange_to]").val();

            if ( (dfrom.length === 0) || (dto.length === 0) ) {
              alert('Specify date range');
              return false;
            }

            var dofrom = getDateObject(dfrom);
            var doto = getDateObject(dto);

            if (!(dofrom.getTime() < doto.getTime())) {
              alert('From date must be earlier than to');
              return false;
            }
            
          }

          $("#viewstatement").attr('action', window.location.href);
          return true;
        });
        break;
    }
  }
});
function toggle_send_on() {
  if ($('#enable_schedule').is(':checked')) {
    $('#sending_date').prop('disabled', false); 
    if ($('#sending_date').val() == 'fixed') {
      $('#fixed_date_container').show();
    }
  } else {
    $('#sending_date').prop('disabled', true); 
    if ($('#fixed_date_container').is(':visible')) {
      $('#fixed_date_container').hide();
    }
  }
}
function toggle_fixed_date() {
  if ($('#sending_date option:selected').val() == 'fixed') {
    $('#fixed_date_container').show();
  } else {
    $('#fixed_date_container').hide();
  }
  return;
}
function getClientSearchPostUrl() {
  return 'addonmodules.php?module=statements&action=getclients';
}
function toggle_daterange_custom() {
  if ($('#date_range').val() == 'custom_range') {
    $('#daterange_container').show();
  } else {
    $('#daterange_container').hide();
  }
  return;
}
function getDateObject(value) {
    var dparts = value.split("/");
    var d = parseInt(dparts[0], 10),
        m = parseInt(dparts[1], 10),
        y = parseInt(dparts[2], 10);
    return new Date(y, m - 1, d);
}
