<?php
/**
 * Language: English
 */
$_ADDONLANG['display_header']  = 'Display Options';
$_ADDONLANG['group_label']     = 'Group payments';
$_ADDONLANG['group_help']      = 'Display invoice payment directly after the invoice line item. Default is chronological order.';
$_ADDONLANG['ageing_label']    = 'Include ageing';
$_ADDONLANG['ageing_help']     = 'Display the invoice ageing at the bottom of the statement';
$_ADDONLANG['sending_header']  = 'Email Options';
$_ADDONLANG['notice_msgtpl']   = 'To customize the email body see Setup -> Email Templates. Under General Messages find Account Statements';
$_ADDONLANG['attach_label']    = 'Attach invoices';
$_ADDONLANG['attach_help']     = 'Add the invoices that are displayed on the statement as attachments';
$_ADDONLANG['senddate_label']  = 'Send on';
$_ADDONLANG['sendenable_label'] = 'Enable Scheduled Sending';
$_ADDONLANG['sendenable_help']  = 'If enabled will automatically send the current statement on the date selected below.';
$_ADDONLANG['senddate_signup'] = 'Client signup date';
$_ADDONLANG['senddate_fixed']  = 'Fixed date';
$_ADDONLANG['sidebar_title']   = 'Account Statements';
$_ADDONLANG['sidebar_index']   = 'Module Settings';
$_ADDONLANG['sidebar_view']    = 'View Statement';
$_ADDONLANG['view_reference']  = 'Reference';
$_ADDONLANG['view_debit']      = 'Debit';
$_ADDONLANG['view_credit']       = 'Credit';
$_ADDONLANG['invoice']  = 'Invoice';
$_ADDONLANG['itemtype_addfunds']  = 'Credit prefunding';
$_ADDONLANG['itemtype_invoice']  = 'Mass invoice payment';
$_ADDONLANG['opening_balance']  = 'Opening balance';
$_ADDONLANG['daterange_options']['current_month']  = 'Current month';
$_ADDONLANG['daterange_options']['last_month']  = 'Last month';
$_ADDONLANG['daterange_options']['custom_range']  = 'Custom dates';
$_ADDONLANG['ageing_cat5'] = '120+ Days';
$_ADDONLANG['ageing_cat4'] = '90 Days';
$_ADDONLANG['ageing_cat3'] = '60 Days';
$_ADDONLANG['ageing_cat2'] = '30 Days';
$_ADDONLANG['ageing_cat1'] = 'Current';
$_ADDONLANG['ageing_due'] = 'Amount Due';
$_ADDONLANG['ageing_paid'] = 'Amount Paid';
