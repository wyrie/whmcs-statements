<?php
if (!defined("WHMCS")) {
    die("This file cannot be accessed directly");
}

// Require any libraries needed for the module to function.
// require_once __DIR__ . '/path/to/library/loader.php';
//
use WHMCS\Module\Addon\Statements\Admin\AdminDispatcher;
use WHMCS\Module\Addon\Statements\Client\ClientDispatcher;
use WHMCS\Database\Capsule;
use WHMCS\Mail\Template;

function statements_config() {
  return array (
    'name' => 'Account Statements',
    'description' => 'Send, view and display account statements with integrated message, pdf and html templates.',
    'language' => 'english',
    'version'  => '1.0',
    'author'   => 'GS Barr <gsbarr@gmail.com>'
  );
}

/**
 * Activate.
 *
 * @return array Optional success/failure message
 */
function statements_activate()
{
  try {
    Capsule::table('tbladdonmodules')
      ->insert(array(
        array('module' => 'statements', 'setting' => 'group_payments', 'value' => ''),
        array('module' => 'statements', 'setting' => 'include_ageing', 'value' => 'on'),
        array('module' => 'statements', 'setting' => 'attach_invoices', 'value' => ''),
        array('module' => 'statements', 'setting' => 'enable_schedule', 'value' => 'on'),
        array('module' => 'statements', 'setting' => 'sending_date', 'value' => 'signup'),
        array('module' => 'statements', 'setting' => 'fixed_date', 'value' => ''),
      )
    );

    $_mt = new Template;
    $_mt->type = 'general';
    $_mt->name = 'Account Statements';
    $_mt->subject = 'Statement of account as at {$date}';
    $_mt->plaintext = true;
    $_mt->custom = true;
    $_mt->message = <<<EOF
Dear Valued Client

Attached please find your statement of account as at {\$date}.

Yours sincerely
{\$company_name}
EOF;

    $_mt->save();

  } catch (Exception $e) {
    logActivity('Error activating statements module: ' . $e->getMessage());
    return array(
      'status' => 'error',
      'description' => 'Failed activating this module. Please see the activity log for errors.',
    );
  }

  return array(
    'status' => 'success',
  );
}

/**
 * Deactivate.
 *
 * @return array Optional success/failure message
 */
function statements_deactivate()
{
  try {
    Capsule::table('tbladdonmodules')
      ->where('module', 'statements')
      ->delete();

    Template::where([
      ['type', '=', 'general'],
      ['name', '=', 'Account Statements'],
    ])->delete();

  } catch (Exception $e) {
    logActivity('Error deactivating statements module: ' . $e->getMessage());
    return array(
      'status' => 'error',
      'description' => 'Failed deactivating this module. Please see the activity log for errors.',
    );
  }

  return array(
    'status' => 'success',
  );
}

/**
 * Admin Area Output.
 *
 * Called when the addon module is accessed via the admin area.
 * Should return HTML output for display to the admin user.
 *
 * This function is optional.
 *
 * @return string
 */
function statements_output($vars)
{
    $action = isset($_REQUEST['action']) ? $_REQUEST['action'] : '';
    $dispatcher = new AdminDispatcher();
    $response = $dispatcher->dispatch($action, $vars);
    echo $response;
}

function statements_sidebar($vars) {
  $_lang = $vars['_lang'];

  return <<<HTML
<span class="header">
  <img src="images/icons/addonmodules.png" class="absmiddle" width="16" height="16">{$_lang['sidebar_title']}
</span>
<ul class="menu sidebarmenu">
        <li><a href="{$vars['modulelink']}">{$_lang['sidebar_index']}</a></li>
        <li><a href="{$vars['modulelink']}&action=view">{$_lang['sidebar_view']}</a></li>
        <li><a href="#">Version: {$vars['version']}</a></li></ul>
HTML;
}

/**
 * Client Area Output.
 *
 * Called when the addon module is accessed via the client area.
 * Should return an array of output parameters.
 *
 * This function is optional.
 *
 * @return array
 */
function statements_clientarea($vars)
{
    // Get common module parameters
    $modulelink = $vars['modulelink']; // eg. index.php?m=addonmodule
    $version = $vars['version']; // eg. 1.0
    $_lang = $vars['_lang']; // an array of the currently loaded language variables

    return array(
        'pagetitle' => 'Statements',
        'breadcrumb' => array($modulelink => 'Statements'),
        'templatefile' => 'clienthome',
        'requirelogin' => true, # accepts true/false
        'forcessl' => true, # accepts true/false
        'vars' => array(
            'testvar' => 'demo',
            'anothervar' => 'value',
            'sample' => 'test',
        ),
    );
}
